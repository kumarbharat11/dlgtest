from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.types import *

#Load excel file into from path
df1=spark.load(C:\Users\bharat-k\Downloads\Test\df1.xls", format="xls", sep=",", inferSchema="true", header="true")
df2=spark.load(C:\Users\bharat-k\Downloads\Test\df2.xls", format="xls", sep=",", inferSchema="true", header="true")

#Write file into parquet format	
df1.write.parquet(C:\Users\bharat-k\Downloads\Test\df1.parquet")
df2.write.parquet(C:\Users\bharat-k\Downloads\Test\df1.parquet")

#Read file in parquet file format
parquetfile1=spark.read.parquet(C:\Users\bharat-k\Downloads\Test\df1.parquet")
parquetfile2.spark.read.parquet(C:\Users\bharat-k\Downloads\Test\df2.parquet")

#Create table
parquetfile1.createOrReplaceTempView("day1")
parquetfile2.createOrReplaceTempView("day2")
#SQL query to fetch data 

Q1=spark.sql(select ObservationDate from day1 where ScreenTemperature in (select max(ScreenTemperature) from day1)
Q2=spark.sql(select ScreenTemperature from day1 where ScreenTemperature in (select max(ScreenTemperature) from day1)
Q3=spark.sql(select Region from day1 where ScreenTemperature in (select max(ScreenTemperature) from day1)
Q1.show()
Q2.show()
Q3.show()


Unit Test case
===============
assert q1 == 50
#if condition returns True, test case pass:
assert q1 != 45
#if condition returns False, AssertionError is raised:
assert q2 == 37.5
#if condition returns True, test case pass
assert q2 != ?
#if condition returns False, AssertionError is raised:
assert q3 == ?
#if condition returns True, test case pass:
assert q3 != ?
#if condition returns False, AssertionError is raised:

Documentation
===============

1. I received data in Excel format. 
2. I converted Excel file into parquet format. 
3. Read file in parquet file format. 
4. Created table to run sql queries.





